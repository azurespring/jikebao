<?php

namespace AzureSpring\Jikebao\Notification;

class VerificationNotification extends OrderNotification
{
    /** @var int */
    private $quantity;

    /** @var string */
    private $transTime;

    public static function support(array $params)
    {
        return !array_diff(['orderId', 'verifyNum', 'totalVerifyNum', 'transTime'], array_keys($params));
    }

    public static function compose(array $params)
    {
        return new self($params['orderId'], $params['totalVerifyNum'], $params['transTime']);
    }

    public function __construct(string $orderId, int $quantity ,string $transTime)
    {
        parent::__construct($orderId);

        $this->quantity = $quantity;
        $this->transTime = $transTime;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return string
     */
    public function getTransTime(): string
    {
        return $this->transTime;
    }
}
