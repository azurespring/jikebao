<?php

namespace AzureSpring\Jikebao\Notification;

class IssueNotification extends OrderNotification
{
    /** @var string */
    private $code;

    /** @var string[] */
    private $urls;

    public static function support(array $params)
    {
        return !array_diff(['orderId', 'eticket', 'qrcodeUrl'], array_keys($params));
    }

    public static function compose(array $params)
    {
        return new self(
            $params['orderId'],
            $params['eticket'],
            array_filter(array_map('trim', explode(',', $params['qrcodeUrl'])))
        );
    }

    public function __construct(string $orderId, string $code, array $urls)
    {
        parent::__construct($orderId);

        $this->code = $code;
        $this->urls = $urls;

    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string[]
     */
    public function getUrls(): array
    {
        return $this->urls;
    }
}
