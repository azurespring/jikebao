<?php

namespace AzureSpring\Jikebao\Notification;

class RefundNotification extends OrderNotification
{
    const STATE_APPROVED = 1;
    const STATE_REJECTED = 0;

    /** @var string */
    private $referenceId;

    /** @var bool */
    private $approved;

    /** @var int */
    private $quantity;

    public static function support(array $params)
    {
        return !array_diff(['refundNum', 'orderId', 'serialNo', 'refundStatus'], array_keys($params));
    }

    public static function compose(array $params)
    {
        return new self(
            $params['orderId'],
            $params['serialNo'],
            self::STATE_APPROVED === (int) $params['refundStatus'],
            $params['refundNum']
        );
    }

    public function __construct(string $orderId, string $referenceId, bool $approved, int $quantity)
    {
        parent::__construct($orderId);

        $this->referenceId = $referenceId;
        $this->approved = $approved;
        $this->quantity = $quantity;
    }

    /**
     * @return string
     */
    public function getReferenceId(): string
    {
        return $this->referenceId;
    }

    /**
     * @return bool
     */
    public function isApproved(): bool
    {
        return $this->approved;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }
}
