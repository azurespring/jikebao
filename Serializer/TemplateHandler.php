<?php

namespace AzureSpring\Jikebao\Serializer;

use AzureSpring\Jikebao\Annotation\Template;
use Doctrine\Common\Annotations\AnnotationReader;
use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigatorInterface;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonDeserializationVisitor;

class TemplateHandler implements SubscribingHandlerInterface
{
    private $frames = [];

    private $reader;

    public static function getSubscribingMethods()
    {
        return [
            [
                'direction' => GraphNavigatorInterface::DIRECTION_DESERIALIZATION,
                'format' => 'json',
                'type' => 'template',
                'method' => 'deserialize',
            ],
        ];
    }

    public function __construct()
    {
        $this->reader = new AnnotationReader();
    }

    public function deserialize(JsonDeserializationVisitor $visitor, $data, array $type, Context $context)
    {
        $types = current($this->frames) ?: [];
        $bind = function (array $type) use (&$bind, $types) {
            if (array_key_exists($name = $type['name'], $types)) {
                return $types[$name];
            }

            return [
                'name' => $name,
                'params' => array_map($bind, $type['params']),
            ];
        };

        $type = $bind(current($type['params']));
        $frame = [];
        if (class_exists($type['name'])) {
            $class = new \ReflectionClass($type['name']);

            /** @var Template $template */
            if ($template = $this->reader->getClassAnnotation($class, Template::class)) {
                $frame = array_combine($template->params, $type['params']);
            }
        }

        $this->frames[] = $frame;
        $data = $context->getNavigator()->accept($data, $type, $context);
        array_pop($this->frames);

        return $data;
    }
}
