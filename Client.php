<?php

namespace AzureSpring\Jikebao;

use AzureSpring\Jikebao\Annotation\Template;
use AzureSpring\Jikebao\Notification;
use AzureSpring\Jikebao\Model\Order;
use AzureSpring\Jikebao\Model\OrderOptions;
use AzureSpring\Jikebao\Model\Paginated;
use AzureSpring\Jikebao\Model\PaginatedResult;
use AzureSpring\Jikebao\Model\Result;
use AzureSpring\Jikebao\Model\Product;
use AzureSpring\Jikebao\Model\Refund;
use AzureSpring\Jikebao\Model\SingleResult;
use AzureSpring\Jikebao\Serializer\TemplateHandler;
use GuzzleHttp\Client as Guzzle;
use JMS\Serializer\Handler\HandlerRegistryInterface;
use JMS\Serializer\SerializerBuilder;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Symfony\Component\Mime\MimeTypes;


class Client
{
    private $guzzle;

    private $key;

    private $secret;

    private $serializer;

    private $contents;

    private $responseFactory;

    private $streamFactory;

    public function __construct(Guzzle $guzzle, string $key, string $secret, ResponseFactoryInterface $responseFactory, StreamFactoryInterface $streamFactory)
    {
        $this->guzzle = $guzzle;
        $this->key = $key;
        $this->secret = $secret;
        $this->serializer = SerializerBuilder::create()
            ->setMetadataDirs([
                'AzureSpring\\Jikebao\\Model' => __DIR__.'/Resources/config/serializer',
            ])
            ->configureHandlers(function (HandlerRegistryInterface $registry) {
                $registry->registerSubscribingHandler(new TemplateHandler());
            })
            ->addDefaultHandlers()
            ->build()
        ;
        $this->responseFactory = $responseFactory;
        $this->streamFactory = $streamFactory;
    }

    public function findProducts(): Paginated
    {
        return $this->request(Template::bind(PaginatedResult::class, Product::class), 'productList');
    }

    public function findProduct(int $id): ? Product
    {
        $result = $this->request(Template::bind(PaginatedResult::class,Product::class), 'getProductInfo', ['productId' => $id]);
        return current($result->getData());
    }

    public function create(int $product, OrderOptions $options, int $quantity = 1): Order
    {
        return $this
            ->request(
                Template::bind(SingleResult::class, Order::class),
                'createOrder',
                [
                    'resourceId' => $product,
                    'orderQuantity' => $quantity,
                ] +
                $options->toParams()
            )
            ->getData()
        ;
    }

    public function clear(string $id)
    {
        $this->request(Template::bind(SingleResult::class, Order::class), 'payOrder', ['orderId' => $id]);
    }

    public function remove(string $id, ?int $quantity = null): Refund
    {
        return $this
            ->request(
                Template::bind(SingleResult::class, Refund::class),
                'orderRefund',
                array_filter([
                    'orderId' => $id,
                    'refundQuantity' => $quantity
                ])
            )
            ->getData()
        ;
    }

    public function getContents(): ?string
    {
        return $this->contents;
    }

    private function request(string $type, string $method, array $params = [])
    {
        $form = null;
        $query = [
            'method' => $method,
        ];

        $form = json_encode(['appKey' => $this->key, 'body' => $params]);
        $ts = (int) (microtime(true) * 1000);
        $headers = [
            'TIMESTAMP' => $ts,
            'APPKEY'     => $this->key,
            'SIGN'     => $this->signReq($form ,$ts),
            'Content-Type'     => 'application/json;charset=utf-8'
        ];

        $response = $this->guzzle->request('POST', '', ['headers' =>$headers, 'query' => $query, 'body' => $form]);
        $this->contents = $response->getBody()->getContents();

        /** @var SingleResult|PaginatedResult $result */
        $result = $this->serializer->deserialize($this->contents, $type, 'json');
        if ($result->getCode() !== 200) {
            throw new Exception($result->getMessage(), $result->getCode());
        }

        return $result;
    }

    private function signReq($form ,$ts)
    {
        $pv =[];
        array_unshift($pv, $this->key);
        array_push($pv, $form);
        array_push($pv, $ts);
        array_push($pv, $this->secret);
        return strtoupper(md5(implode('', $pv)));
    }

    private function signRecv(array $params)
    {
        if(isset($params['qrcodeUrl'])) {
            $params['qrcodeUrl'] = urlencode($params['qrcodeUrl']);
        }
        if(isset($params['transTime'])) {
            $params['transTime'] = urlencode($params['transTime']);
        }
        $pv = array_map(
            function ($key, $value) {
                return "{$key}={$value}";
            },
            array_keys($params),
            array_values($params)
        );
        sort($pv, SORT_STRING);
        return strtoupper(md5(implode('&', array_filter($pv)).$this->secret));
    }

    public function recv(ServerRequestInterface $request)
    {
        $params = array_filter($request->getQueryParams());
        $sign = @$params['sign'];
        unset($params['sign']);
        if ($this->key !== $params['appkey'] || $this->signRecv($params) !== $sign) {
            return null;
        }
        /** @var Notification\AbstractNotification $class */
        foreach ([
                     Notification\RefundNotification::class,
                     Notification\VerificationNotification::class,
                     Notification\IssueNotification::class,
                 ] as $class) {
            if ($class::support($params)) {
                dump($params);
                return $class::compose($params);
            }
        }
        return null;
    }

    public function ackn(): ResponseInterface
    {
        return $this
            ->responseFactory
            ->createResponse()
            ->withHeader('Content-Type', 'text/plain ')
            ->withBody($this->streamFactory->createStream('success'));
    }


}
