<?php

namespace AzureSpring\Jikebao\Model;

class Refund
{
    const STATUS_NEW = 0;
    const STATUS_DONE = 1;
    const STATUS_REJECTED = 2;

    /** @var int */
    private $id;
    
    /** @var int */
    private $status;

    /** @var string */
    private $serialNo;
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getSerialNo(): string
    {
        return $this->serialNo;
    }
}
