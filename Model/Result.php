<?php

namespace AzureSpring\Jikebao\Model;

abstract class Result
{
    /**
     * @var int|null
     */
    private $code;

    /**
     * @var string|null
     */
    private $message;
    
    /**
     * @return string|null
     */
    public function getCode(): ?int
    {
        return $this->code;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }
}
