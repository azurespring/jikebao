<?php

namespace AzureSpring\Jikebao\Annotation;

/**
 * @Annotation
 * @Target({"CLASS"})
 */
final class Template
{
    public static function bind(string $name, string ...$params)
    {
        return sprintf('template<%s<%s>>', $name, implode(', ', $params));
    }

    /**
     * @var array<string>
     * @Required
     */
    public $params;
}
